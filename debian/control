Source: libxml-smart-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar Burchardt <ansgar@debian.org>,
           Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libdevel-cycle-perl,
                     libobject-multitype-perl,
                     perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libxml-smart-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libxml-smart-perl.git
Homepage: https://metacpan.org/release/XML-Smart

Package: libxml-smart-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libobject-multitype-perl
Multi-Arch: foreign
Description: Perl module for access to parsed XML trees
 With the XML::Smart module, nodes in an XML tree made with XML::LibXML can
 be accessed equally conveniently with the Perl syntax for hashes and arrays.
 It also provides convenience features such as searching for nodes by
 attribute, selection of an attribute value in each of multiple nodes,
 change the format of returned data, and so forth.
 .
 The module also professes to automatically handle binary data
 (encoding/decoding to/from base64), CDATA sections (used for text
 content with unbalanced <tags> and so forth), and Unicode.  It can be
 used to create XML files and load XML from the Web (just pasting an URL
 as a file path).  It also provides an easy way to send XML data through
 sockets, adding the length of the data in the <?xml?> header.
